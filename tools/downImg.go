package tools

import (
	"bytes"
	"context"
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/elastic/go-elasticsearch/v7"
)

func httpGetImg(url string, raw []byte) error {
	resp, err := http.Get(url)

	if err != nil {
		return err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	raw = body

	return nil
}

func downloadImg(url string) {

	imgCont := []byte{}
	err := httpGetImg(url, imgCont)
	if err != nil {
		return
	}

	SaveFileName := createFileName("/home/img/", url)

	newFileHandler, err := os.Create(SaveFileName)
	if err != nil {
		panic(err)
	}
	defer newFileHandler.Close()

	_, err = newFileHandler.Write(imgCont)

	if err != nil {
		panic(err)
	}

}

func createFileName(saveDir, srcImg string) string {

	//后缀
	extArr := strings.Split(srcImg, ".")
	ext := extArr[len(extArr)-1]

	//生成规则
	hash := sha1.New()
	hash.Write([]byte(srcImg))
	randFileNameStr := fmt.Sprintf("%x", hash.Sum(nil))
	saveDir = fmt.Sprintf("%s/%x/%x/%x/", saveDir, randFileNameStr[0], randFileNameStr[len(randFileNameStr)-1], randFileNameStr[len(randFileNameStr)])

	fileName := saveDir + randFileNameStr + "." + ext
	return fileName
}

func DoDownImg() {

	var (
		r   map[string]interface{}
		err error
		wg  sync.WaitGroup
	)
	handleCH := make(chan map[string]interface{}, 5000)

	cfg := elasticsearch.Config{
		Addresses: []string{
			"http://192.168.1.125:9200",
		},
	}
	es, err := elasticsearch.NewClient(cfg)

	if err != nil {
		fmt.Printf("Error Establishing Es: %s", err)
		os.Exit(1)
	}

	//通过管道进行任务处理
	go func() {
		//传输进来数据
		//log.Printf(" * ID=%s, %s", hit.(map[string]interface{})["_id"], hit.(map[string]interface{})["_source"])
		for v := range handleCH {
			url := "v中取出来"
			//图片别名hash,hash首字符和末尾两字符拼接成字符串
			saveFileName := createFileName("/home/img/", url)
			//url地址下载保存到本地
			downloadImg(url)
			//成功后数据保存

			esCli, err := elasticsearch.NewClient(cfg)

			if err != nil {
				fmt.Printf("Error Establishing Es: %s", err)
				os.Exit(1)
			}

		}

	}()

	var buf bytes.Buffer

	query := map[string]interface{}{
		"query": map[string]interface{}{
			"match_all": map[string]interface{}{},
		},
		"_source": []string{"img", "procedures"},
		"size":    1,
	}

	if err := json.NewEncoder(&buf).Encode(query); err != nil {
		log.Fatalf("Error encoding query: %s", err)
	}

	//fmt.Println(buf.String())

	// Perform the search request.
	res, err := es.Search(
		es.Search.WithContext(context.Background()),
		es.Search.WithIndex("cuisines"),
		es.Search.WithBody(&buf),
		es.Search.WithScroll(1*time.Minute),
		es.Search.WithTrackTotalHits(true),
		es.Search.WithPretty(),
	)
	if err != nil {
		log.Fatalf("Error getting response: %s", err)
	}
	defer res.Body.Close()

	if res.IsError() {
		var e map[string]interface{}
		if err := json.NewDecoder(res.Body).Decode(&e); err != nil {
			log.Fatalf("Error parsing the response body: %s", err)
		} else {
			// Print the response status and error information.
			log.Fatalf("[%s] %s: %s",
				res.Status(),
				e["error"].(map[string]interface{})["type"],
				e["error"].(map[string]interface{})["reason"],
			)
		}
	}

	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		log.Fatalf("Error parsing the response body: %s", err)
	}

	scrollId := r["_scroll_id"].(string)

	for _, hit := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {

		wg.Add(1)
		handleCH <- hit.(map[string]interface{})
	}

	for {

		res, err := es.Scroll(
			es.Scroll.WithScrollID(scrollId),
			es.Scroll.WithScroll(1*time.Minute),
		)

		if err != nil {

			fmt.Printf("Error Scroll Es: %s", err)
			os.Exit(1)
		}

		r = nil

		if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
			log.Fatalf("Error parsing the response body: %s", err)
		}

		if r == nil {
			break
		}

		for _, hit := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {

			wg.Add(1)
			handleCH <- hit.(map[string]interface{})

		}

	}

}
