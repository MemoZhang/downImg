package tools

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/elastic/go-elasticsearch/esapi"
	"github.com/elastic/go-elasticsearch/v7"
	"github.com/gocolly/colly"
	"github.com/gocolly/colly/extensions"
	"github.com/gomodule/redigo/redis"
)

type Material struct {
	MaterialName string `json:"materialname"`
	Weight       string `json:"weight"`
}

type Procedure struct {
	MethodImg  string `json:"methodimg"`
	MethodCont string `json:"methodcont"`
}

type Effect struct {
	Materials []string
	Content   string
}

var Effects = make([]Effect, 0)

var RedisPool *redis.Pool

//分类划定 二类->一类,三类->二类,菜名->三类

var catgoryCh = make(chan map[string]string, 100)
var foodCh = make(chan map[string]string, 100)
var EsCh = make(chan Cuisine, 100)

var globalUseMux sync.WaitGroup

type Cuisine struct {
	Name          string      `json:"dishes"`
	Img           string      `json:"img"`
	LocalImg      string      `json:"Localimg"`
	FirstCatgory  string      `json:"catgory"`
	SecondCatgory string      `json:"subcatgory"`
	ThirdCatgory  string      `json:"thirdcatgory"`
	Materials     []Material  `json:"materials"`
	Procedures    []Procedure `json:"procedures"`
	Effect        string      `json:"effect"`
}

func RedisPoolInit() error {

	RedisPool = &redis.Pool{
		MaxIdle:     500,
		MaxActive:   500,
		IdleTimeout: 240 * time.Second,
		Wait:        true,
		Dial:        newRedisConn,
	}

	redisInstance := RedisPool.Get()
	defer redisInstance.Close()

	_, err := redisInstance.Do("Ping")

	if err != nil {
		return err
	}

	return nil
}

func newRedisConn() (redis.Conn, error) {

	//获取redis配置

	redisConn, err := redis.Dial("tcp", "192.168.1.125:6379")
	if err != nil {
		return nil, err
	}

	return redisConn, nil
}

func InitEffect() {
	fd, err := os.Open("/Users/zhangpeimeng/software/matchfood/matchfood.txt")
	if err != nil {
		panic(err)
	}
	defer fd.Close()
	bfRd := bufio.NewReader(fd)

	for {

		line, err := bfRd.ReadBytes('\n')
		single := strings.Trim(string(line), "\n")

		contentArr := strings.Split(single, "：")
		materialArr := strings.Split(contentArr[0], "与")

		Effects = append(Effects, Effect{materialArr, single})

		if err != nil {
			if err == io.EOF {
				break
			}
			panic(err)
		}

	}

}

//匹配功效
func MatchEffect(materials []Material) string {

	for _, effect := range Effects { //功效食材切片
		adeueateNum := 0
		for _, effectMaterial := range effect.Materials { //功效食材切片

			for _, material := range materials {

				if material.MaterialName == effectMaterial {
					adeueateNum++
				}
			}
		}

		if adeueateNum == len(effect.Materials) {
			return effect.Content
		}

	}

	return ""

}

//es插入
func InsertEs() {

	cfg := elasticsearch.Config{
		Addresses: []string{
			"http://192.168.1.125:9200",
		},
	}
	es, err := elasticsearch.NewClient(cfg)

	if err != nil {
		fmt.Printf("Error Establishing Es: %s", err)
		os.Exit(1)
	}

	for cuisine := range EsCh {

		data, _ := json.Marshal(cuisine)

		//fmt.Println(string(data))

		req := esapi.IndexRequest{
			Index:   "cuisines",
			Body:    strings.NewReader(string(data)),
			Refresh: "true",
		}

		res, err := req.Do(context.Background(), es)
		if err != nil {
			fmt.Printf("Error getting response: %s", err)
		}

		res.Body.Close()

	}
}

func BeginSpider(wg *sync.WaitGroup) {

	begin := time.Now()

	//读取功效txt写入内存。
	InitEffect()

	RedisPoolInit()

	//食材管道去读

	for i := 0; i <= 5; i++ {
		go func() {
			redisInstance := RedisPool.Get()

			for maps := range catgoryCh {

				for key, val := range maps {
					_, err := redisInstance.Do("SetEx", key, 1800, val)
					fmt.Println(key, "-", val)
					if err != nil {
						fmt.Println(err)
					}
				}
			}
			redisInstance.Close()
		}()
	}

	//食物管道去读
	for i := 0; i <= 5; i++ {

		go func() {
			redisInstance := RedisPool.Get()

			for maps := range foodCh {

				for key, val := range maps {
					_, err := redisInstance.Do("SetEx", key, 1800, val)
					fmt.Println(key, "-", val)
					if err != nil {
						fmt.Println(err)
					}
				}
			}
			redisInstance.Close()

		}()
	}

	//携程ES
	for i := 0; i < 5; i++ {
		go InsertEs()
	}

	//爬虫准备工作
	c := colly.NewCollector(func(c *colly.Collector) {
		extensions.RandomUserAgent(c)
		c.Async = true

	},
		colly.URLFilters(
			regexp.MustCompile("^https://www.douguo.com/shicai"),
			regexp.MustCompile("^http(s)?://www.douguo.com/ingredients"),
			regexp.MustCompile("^http(s)?://www.douguo.com/cookbook"),
		),
	)

	c.Limit(&colly.LimitRule{DomainGlob: "*.douguo.com", Parallelism: 5})

	c.OnRequest(func(r *colly.Request) {
		// r.Ctx.Put("url", r.URL.String())
		//fmt.Println("Visiting :", r.URL.String())
	})

	//食材页面 获取食材更多连接
	c.OnHTML("div.wrap div#content:not(.clearfix) div.food-type.clearfix", func(e *colly.HTMLElement) {

		var firstCat, secondCat string
		//第一个
		var catmap = make(map[string]string)

		e.ForEach("div", func(_ int, ele *colly.HTMLElement) {

			if ele.DOM.HasClass("title-head") {
				//大类first
				firstCatTmp := ele.DOM.Find("h3").Text()
				firstCatArr := strings.Split(firstCatTmp, "(")
				firstCat = firstCatArr[0]

			} else if ele.DOM.HasClass("type clearfix") {
				second := ele.DOM.Children().First()
				secondCat = second.Text()
				if secondCat != "" {
					catmap[secondCat] = firstCat
				}

			}

		})

		catgoryCh <- catmap

		e.ForEach("li.more a[href]", func(_ int, elem *colly.HTMLElement) {

			link := elem.Attr("href")
			globalUseMux.Add(1)

			c.Visit(elem.Request.AbsoluteURL(link))

		})

		// fmt.Println(Catgory)

	})

	// //食材分类详情页面
	c.OnHTML("div.wrap div#content.clearfix div.food-type.clearfix.mh600", func(e *colly.HTMLElement) {

		var catmap = make(map[string]string)
		second := e.DOM.Find("div.type h4 a").Text()

		e.ForEach("ul.clearfix li a", func(_ int, thirdele *colly.HTMLElement) {
			catmap[thirdele.DOM.Text()] = second
		})
		catgoryCh <- catmap

		e.ForEach("ul.clearfix a[href]", func(_ int, elem *colly.HTMLElement) {
			link := elem.Attr("href")
			c.Visit(e.Request.AbsoluteURL(link))
		})

	})

	//食材分子具体食物列表
	c.OnHTML("div.wrap div.clearfix#content div#left", func(e *colly.HTMLElement) {

		//第三类
		thirdElment := e.DOM.Find("div.material.clearfix.mt20 div.des-material h3")
		thirdcatgory := thirdElment.Text()

		//详细做法
		e.ForEach("ul.cook-list li.clearfix", func(_ int, e *colly.HTMLElement) {

			var foodmap = make(map[string]string)

			Name, _ := e.DOM.Find("img").Attr("title")

			foodmap[Name] = thirdcatgory
			foodCh <- foodmap

			link, _ := e.DOM.Find("a.cook-img").Attr("href")

			c.Visit(e.Request.AbsoluteURL(link))
		})

	})

	// 食材分子具体食物列表
	c.OnHTML("div.wrap div.clearfix div.mt20 div.pages", func(e *colly.HTMLElement) {

		is_end := false

		e.ForEach("a", func(_ int, ele *colly.HTMLElement) {
			if ele.DOM.HasClass("anext") {
				//食物列表分页
				link := ele.Attr("href")
				is_end = true
				c.Visit(e.Request.AbsoluteURL(link))
			}
		})

		if !is_end {
			globalUseMux.Done()
		}

	})

	// //获取菜谱详细做法
	c.OnHTML("div#content.recipe-content.clearfix", func(e *colly.HTMLElement) {

		//略缩图
		imgElement := e.DOM.Find("div.relative div#banner a").First()
		titleImg, _ := imgElement.Attr("href")

		//菜名
		cuisineName := e.DOM.Find("div.rinfo.relative h1.title.text-lips").Text()

		//原材料
		materialsElements := e.DOM.Find("div.metarial table tbody tr")

		materials := []Material{}

		materialsElements.Each(func(_ int, selection *goquery.Selection) {

			td := selection.Find("td").Next()
			MaterialName := td.Find("span.scname").Text()
			Weight := td.Find("span.right.scnum").Text()

			materials = append(materials, Material{MaterialName, Weight})

		})

		//步骤
		procedures := []Procedure{}

		e.DOM.Find("div.step div.stepcont.clearfix").Each(func(_ int, selection *goquery.Selection) {
			MethodImg, _ := selection.Find("a[rel]").Attr("href")
			MethodContTmp := strings.Fields(selection.Find("div.stepinfo").Text())

			if len(MethodContTmp) > 1 {
				procedures = append(procedures, Procedure{MethodImg, MethodContTmp[1]})
			} else {
				procedures = append(procedures, Procedure{MethodImg, MethodContTmp[0]})

			}

		})

		redisInstance := RedisPool.Get()

		cuisine := Cuisine{}
		cuisine.Name = cuisineName
		cuisine.Img = titleImg
		cuisine.Materials = materials
		cuisine.Procedures = procedures
		cuisine.Effect = MatchEffect(materials)

		var err error

		cuisine.ThirdCatgory, err = redis.String(redisInstance.Do("Get", cuisineName))
		if err != nil {
			fmt.Println(err)
		}
		cuisine.SecondCatgory, err = redis.String(redisInstance.Do("Get", cuisine.ThirdCatgory))
		if err != nil {
			fmt.Println(err)
		}
		cuisine.FirstCatgory, err = redis.String(redisInstance.Do("Get", cuisine.SecondCatgory))
		if err != nil {
			fmt.Println(err)
		}
		redisInstance.Close()

		fmt.Println(cuisineName)

		//开启协程进行es写入
		EsCh <- cuisine

	})

	c.OnResponse(func(r *colly.Response) {
		// url := r.Ctx.Get("url")
		// fmt.Println("Done visiting", url)
	})

	c.Visit("https://www.douguo.com/shicai")
	c.Wait()

	globalUseMux.Wait()

	wg.Done()

	fmt.Printf("comsume time: %v \n", time.Since(begin))

}
