package main

import (
	"flag"
	"fmt"
	"os"
	"sync"

	"spider/tools"
)

func main() {

	var selectType string
	var wg sync.WaitGroup
	flag.StringVar(&selectType, "mode", "", "选择模式:spider|web|build|downimg")
	flag.Parse()

	switch selectType {
	case "spider":
		wg.Add(1)
		tools.BeginSpider(&wg)
		wg.Wait()
		fmt.Println("爬虫任务结束")
	case "web": //查询服务
	case "build": //生成静态模板
	case "downimg": //下载图片
		tools.DoDownImg()
	default:
		fmt.Println("不支持其他模式")
		os.Exit(1)
	}

}
